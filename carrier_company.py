#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Eval, If, In

import logging
import datetime
from time import time

__all__ = ['CarrierShipping']

STATES = [
    ('draft','Draft'),
    ('validated','Validated'),
    ('confirmed','Confirmed'),
    ('done','Done'),
    ('cancel','Canceled')
    ]

class CarrierShipping(Workflow,ModelSQL, ModelView):
    'Carrier Shipping Resource Planning'
    __name__ = 'carrier.shipping'
    _rec_name = 'reference'

    reference = fields.Char('Reference', readonly=True, required=True, 
                            select=True)

    date_from = fields.Date('date_from', states={                                                                          
        'readonly': Eval('state').in_(['validated', 'confirmed']),
        'required': Eval('state').in_(['validated', 'confirmed']),                                                           
    }, depends=['state'])

    time_from = fields.Time('time_from', states={                                                                       
        'readonly': Eval('state').in_(['validated', 'confirmed']),                                                          
    }, depends=['state'])

    zip_from = fields.Many2One('country.zip', 'Zip', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    city_from = fields.Char('city_from', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    street_from = fields.Char('street_from', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    streetbis_from = fields.Char('streetbis_from', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    subdivision_from = fields.Many2One("country.subdivision",'Subdivision', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    country_from = fields.Many2One('country.country', 'Country', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    date_to = fields.Date('date_to', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    time_to = fields.Time('time_to', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    zip_to = fields.Many2One('country.zip', 'Zip', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state']) 
    
    city_to = fields.Char('city_to', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    street_to = fields.Char('street_to', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    streetbis_to = fields.Char('streetbis_to', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    subdivision_to = fields.Many2One("country.subdivision",'Subdivision', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    country_to = fields.Many2One('country.country', 'Country', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    distance = fields.Integer('distance', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    volume = fields.Integer('volume', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    weight = fields.Integer('weight', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    price = fields.Numeric('price',digits=(16,4),required=True, states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    method_cost = fields.Selection([('',''),('P','Percentage'),('F','Fixed')],'method_cost', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    method_cost_value = fields.Numeric('method_cost_value',digits=(16,4), states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    carrier = fields.Many2One('carrier', 'Carrier', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])
    
    party = fields.Many2One('party.party', 'Party', required=True, states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),
    }, depends=['state'])

    state = fields.Selection(STATES, 'State', readonly=True, select=True)
    
    reference_invoice = fields.Char('Reference Invoice', states={
        'readonly': Eval('state').in_(['validated', 'confirmed']),                                                            
    }, depends=['state'])
    
    note = fields.Char('Note', states={                                                                         
        'readonly': Eval('state').in_(['validated', 'confirmed']),                                                            
    }, depends=['state'])
    
    sale = fields.Many2One('sale.sale', 'Sale', readonly=True)
    
    purchase = fields.Many2One('purchase.purchase', 'Purchase', readonly=True)    

    @classmethod
    def __setup__(cls):
        super(CarrierShipping, cls).__setup__()

        cls._transitions |= set((
                ('draft', 'validated'),
                ('validated', 'confirmed'),
                ('confirmed', 'done'),
                ('validated', 'cancel'),
                ('validated', 'draft'),
                ('draft', 'cancel'),
                ('cancel', 'draft'),
                ))

        cls._buttons.update({
                'draft': {
                    'invisible': ~Eval('state').in_(['validated','cancel']),
                    },
                'validated': {
                    'invisible': ~Eval('state').in_(['draft']),
                    },
                'confirmed': {
                    'invisible': ~Eval('state').in_(['validated']),
                    },
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft', 'validated']),
                    },
                })

    @classmethod
    def default_state(cls):
        return 'draft'
    
    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('carrier.configuration')
        sequence = Config(1).carrier_shipping_sequence
        
        logging.getLogger('carrier_company').info(
             sequence.id)
        
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            vals['reference'] = Sequence.get_id(sequence.id)

        return super(CarrierShipping, cls).create(vlist)

    @classmethod
    def copy(cls, shippings, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('sale', None)
        default.setdefault('purchase', None)
        default.setdefault('reference_invoice', None)
        return super(CarrierShipping, cls).copy(shippings, default=default)

    @fields.depends('carrier')
    def on_change_carrier(self):
        changes = {}
        if not self.carrier: return changes

        if self.carrier.carrier_cost_method == 'product':
            changes['method_cost'] = 'F'
            changes['method_cost_value'] = self.carrier.carrier_product.list_price
        
        if self.carrier.carrier_cost_method == 'percentage':
            changes['method_cost'] = 'P'
            changes['method_cost_value'] = self.carrier.percentage
            
        return changes
    
    @fields.depends('zip_from')
    def on_change_zip_from(self):
        changes = {}
        if not self.zip_from: return changes
        
        changes['city_from'] = self.zip_from.city
        changes['country_from'] = self.zip_from.country.id
        changes['subdivision_from'] = self.zip_from.subdivision.id
        
        return changes
 
    @fields.depends('zip_to')
    def on_change_zip_to(self):
        changes = {}
        if not self.zip_to: return changes
        
        changes['city_to'] = self.zip_to.city
        changes['country_to'] = self.zip_to.country.id
        changes['subdivision_to'] = self.zip_to.subdivision.id
        
        return changes
    
    def _get_description(self):
        desc = 'Origen : '
        
        if self.date_from:
            desc = desc + self.date_from.strftime('%d/%m/%Y') + ' '
            
        if self.city_from:
            desc = desc + self.city_from + ' '
            
        if self.subdivision_from:
            desc = desc + self.subdivision_from.name + ' '            
            
        desc = desc + '\nDestino : '

        if self.date_to:
            desc = desc + self.date_to.strftime('%d/%m/%Y') + ' '

        if self.city_to:
            desc = desc + self.city_to + ' '

        if self.subdivision_to:
            desc = desc + self.subdivision_to.name + ' '

        return desc
        
    def _get_sale_line_shipping(self):
        '''
        Return sale line for a shipping
        '''
        SaleLine = Pool().get('sale.line')
        with Transaction().set_user(0, set_context=True):
            return SaleLine(
                unit_price = self.price,
                product = self.carrier.carrier_product,
                taxes  = self.carrier.carrier_product.template.customer_taxes,
                quantity = 1,                
                description = self._get_description(),
                unit = self.carrier.carrier_product.sale_uom,
                delivery_date = self.carrier.carrier_product.compute_delivery_date(),
                note = self.reference_invoice,
                )

    def _get_sale_shipping(self):
        '''
        Return sale for a shipping
        '''
        Sale = Pool().get('sale.sale')
        with Transaction().set_user(0, set_context=True):
            return Sale(
                party = self.party,
				comment = self.note,
                sale_date = self.date_from,
                payment_term = self.party.customer_payment_term,
                invoice_address = self.party.address_get(type='invoice'),
                shipment_address = self.party.address_get(type='delivery'),
                reference = self.city_from + ' / ' + self.city_to + ' (' + self.reference + ')' 
                )

    def create_sale(self):
        sale = self._get_sale_shipping()

        sale_line = self._get_sale_line_shipping()
        sale_line.sale = sale
        sale_line.save()

        sale.save()
        # TODO: Make it configurable
        sale.quote([sale])
        sale.confirm([sale])

        self.write([self], {
                'sale': sale.id,
                })

    def _get_purchase_price(self):
        if self.method_cost == 'F':
            return self.method_cost_value
        
        if self.method_cost == 'P':
            return self.price * self.method_cost_value / 100
        
    def _get_purchase_line_shipping(self):
        '''
        Return purchase line for a shipping
        '''
        PurchaseLine = Pool().get('purchase.line')
        with Transaction().set_user(0, set_context=True):
            return PurchaseLine(
                unit_price = self._get_purchase_price(),
                product = self.carrier.carrier_product,
                quantity = 1,
                description = self._get_description(),
                unit = self.carrier.carrier_product.sale_uom,
                taxes  = self.carrier.carrier_product.template.supplier_taxes,
                delivery_date = self.carrier.carrier_product.compute_delivery_date(),
                note = self.reference
                )

    def _get_purchase_shipping(self):
        '''
        Return purchase for a shipping
        '''
        Purchase = Pool().get('purchase.purchase')
        with Transaction().set_user(0, set_context=True):
            return Purchase(
                reference = self.reference,
                party = self.carrier.party,
                comment = self.note,
                purchase_date = self.date_from,
                invoice_address = self.carrier.party.address_get(type='invoice'),                
                payment_term = self.carrier.party.supplier_payment_term,
                supplier_reference = self.city_from + ' / ' + self.city_to
                )
    
    def create_purchase(self):
        purchase = self._get_purchase_shipping()

        purchase_line = self._get_purchase_line_shipping()
        purchase_line.purchase = purchase
        purchase_line.save()
        
        purchase.save()

        # TODO: Make it configurable
        purchase.quote([purchase])
        purchase.confirm([purchase])
        
        self.write([self], {
                'purchase': purchase.id,
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, shipping):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validated(cls, shipping):
        if not shipping[0].date_from:
            cls.raise_user_error('Falta la fecha del origen', cls.rec_name)
        if not shipping[0].date_to:
            cls.raise_user_error('Falta la fecha del destino', cls.rec_name)
        if not shipping[0].carrier:
            cls.raise_user_error('Falta el transportista', cls.rec_name)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, shipping):
        shipping[0].create_sale()
        shipping[0].create_purchase()
# TODO : Make it configurable
#        Attachment = Pool().get('ir.attachment')
#        if Attachment.search([('resource', '=', str(shipping[0]))]):
#            shipping[0].create_sale()
#            shipping[0].create_purchase()
#        else:
#            cls.raise_user_error('No existe ningun archivo adjunto', cls.rec_name)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, shipping):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, shipping):
        pass
    
