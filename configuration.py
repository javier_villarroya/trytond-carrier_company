#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval, Bool

__all__ = ['Configuration']


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Carrier Configuration'
    __name__ = 'carrier.configuration'
    carrier_shipping_sequence = fields.Property(fields.Many2One('ir.sequence',
            'Carrier Shipping Reference Sequence', domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('code', '=', 'carrier.shipping'),
                ], required=True))
    attachment_in_shipment = fields.Boolean('Attachment in shipment')
