#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .carrier_company import *
from .configuration import *

def register():
    Pool.register(
        CarrierShipping,
        Configuration,
        module='carrier_company', type_='model')
